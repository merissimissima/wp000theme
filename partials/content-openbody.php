<body <?php body_class(); ?>>
<div class="header">
    <div class="header-w">
		<header>
	            <div class="grid">
                    <div class="row">
                        <figure><img src="<?php echo get_template_directory_uri(); ?>/images/logo-header.png" alt="logo"/> </figure>
                        <hgroup>
                            <h1><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>
                            <h2><?php bloginfo('description'); ?></h2>
                        </hgroup>
                    </div>
                </div>
        </header>
    </div>
</div>
<div class="section mainmenu no-mobile">
    <div class="grid">
        <div class="menu-container">
            <nav class="main-menu " >
                <?php wp_nav_menu( 'mainmenu' ); ?>
            </nav>
        </div>
    </div>

</div>


<div class="main-menu only-mobile">
    <div class="grid">
		<section class="ac-container only-mobile">
				<div>
					<input id="ac-1" name="accordion-1" type="checkbox" />
					<label for="ac-1">Menu</label>
					<article class="ac-small">
                        <div class="menu-mainmenu-container">
						<?php wp_nav_menu( 'mainmenu' ); ?>
                        </div>
					</article>
				</div>
		</section>
    </div>
</div>
	
