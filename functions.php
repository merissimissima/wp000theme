<?php
//attiva i menu
register_nav_menus(  );
//attia le sidebar e widget
if ( function_exists('register_sidebar') ){
    register_sidebar();
}
// Register Sidebar
function custom_sidebar()  {

    $args = array(
        'id'            => 'footer',
        'name'          => "Footer Sidebar",
        'description'   => "",
        'before_title'  => '<h2 class="widgettitle footer">',
        'after_title'   => '</h2>',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $args );

}

// Hook into the 'widgets_init' action
add_action( 'widgets_init', 'custom_sidebar' );
add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'furniture',
        array(
            'labels' => array(
                'name' => __( 'Mobili' ),
                'singular_name' => __( 'Mobile' )
            ),
            'public' => true,
            'has_archive' => true,
            'exclude_from_search' => true,
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
        )
    );
}
add_theme_support( 'post-thumbnails' );
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'furniture-main-vertical', 350, 510, true ); // hard crop mode
    add_image_size( 'furniture-main-horizontal', 350, 280, true ); // hard crop mode
    add_image_size( 'furniture-main-square', 350, 350, true ); // hard crop mode
    add_image_size( 'carousel-size', 588, 376, true ); // hard crop mode
}
class timerWidget extends WP_Widget {

    function timerWidget() {
        parent::__construct( false, 'Countdown' );
    }
    function widget( $args, $instance ) {
        extract($args);
        echo $before_widget;
         ?>

        <h1><?php echo $instance['title']; ?> </h1>
        <div id="defaultCountdown"></div>
        <script type="text/javascript">
            $( document ).ready(function() {
                $(function () {
                    austDay = new Date(<?php echo $instance['year']; ?>,<?php echo $instance['month']; ?>,<?php echo $instance['day']; ?>,8,0,0,0);
                    console.log(austDay);
                    $('#defaultCountdown').countdown({until: austDay});
                    $('#year').text(austDay.getFullYear());
                });
            });
        </script>
        <?php

        echo $after_widget;
    }
    function update( $new_instance, $old_instance ) {
        return $new_instance;
    }
    function form( $instance ) {

        $year =   esc_attr($instance['year']);
        $month =   esc_attr($instance['month']);
        $day =   esc_attr($instance['day']);
        ?>
        <p><label for="<?php echo $this->get_field_id('title');?>">
                Titolo: <input class="widefat" id="<?php echo $this->get_field_id('title');?>" name="<?php echo $this->get_field_name('title');?>" type="text" value="<?php echo $title; ?>" />
            </label></p>
        <p><label for="<?php echo $this->get_field_id('year');?>">
                Anno: <input class="widefat" id="<?php echo $this->get_field_id('year');?>" name="<?php echo $this->get_field_name('year');?>" type="text" value="<?php echo $year; ?>" />
            </label></p>
        <p><label for="<?php echo $this->get_field_id('month');?>">
                Mese: <input class="widefat" id="<?php echo $this->get_field_id('month');?>" name="<?php echo $this->get_field_name('month');?>" type="text" value="<?php echo $month; ?>" />
            </label></p>
        <p><label for="<?php echo $this->get_field_id('day');?>">
                Giorno: <input class="widefat" id="<?php echo $this->get_field_id('day');?>" name="<?php echo $this->get_field_name('day');?>" type="text" value="<?php echo $day; ?>" />
            </label></p>
    <?php
    }
}
function my_register_widgets() {
    register_widget( 'timerWidget' );
}
add_action( 'widgets_init', 'my_register_widgets' );

?>