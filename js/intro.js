function introAnimation(){
    $('.concepts').fadeIn('slow',
        function(){
            $('#guitar').animate({
                right: "-20px",
                top: "-50px"
            }, 3000, 'easeOutElastic');

            $('#vinil').animate({
                left: "-220px",
                top: "-200px"
            }, 3000, 'easeOutElastic');
            $('.guitar').fadeIn( 'slow', function(){
                $('.drum').fadeIn( 'slow', function(){
                    $('.loud').fadeIn(function(){
                        $('.drunk').fadeIn();
                    });
                });
            });
            $('.music').fadeIn( 'slow', function(){
                $('.livemusic').fadeIn('slow', function(){
                    $('.hard').fadeIn( 'slow', function(){
                        $('.volume').fadeIn(function(){
                            $('#next_1').fadeIn();
                            $('#hand').animate(
                                {left: '0px'}
                                , 3000, 'easeOutElastic',
                                function(){
                                    $('#hand').effect('pulsate');


                                }
                            );
                        });
                    });
                });

            });
        });
    $('#next_1').click(function(){
        $('#next_1').stop().fadeOut();
        $('#concepts').fadeOut();
        $('#guitar').stop().animate({
            right: "-220px",
            top: "-450px"
        }, 3000, 'easeOutElastic');

        $('#vinil').stop().animate({
            left: "-500px",
            top: "-500px"
        }, 3000, 'easeOutElastic');
        $('#hand').stop().animate(
            {left: '-450px'}
            , 3000, 'easeOutElastic');

        $('.c1').animate({
            bottom: "0"
        }, 800, 'easeInQuint');
        $('.c2').animate({
            left: '50%',
            'margin-left': -$('.c2').width()/2
        }, 800, 'easeInQuint');
        $('.c3').animate({
            right: '50%',
            'margin-right': -$('.c3').width()/2
        }, 800, 'easeInQuint');
        $('.c4').animate({
            bottom: "353px"
        }, 800, 'easeInQuint', function(){
            $('.text').animate(
                {bottom : '450px'},
                800,  'easeInQuint', function(){
                    $('.material_description').fadeIn();
                    $('#next_2').fadeIn();
                }
            );

        });

    });

    $('#next_2').click(function(){
        $('#next_2').stop().fadeOut();
        $('.material_description').stop().fadeOut();
        $('#materials_comp').animate({
            bottom : '-2500px'
        } ,800, 'easeInQuint');
        $('.lovehate').animate({
            left : '12%'

        } ,1000, 'easeInQuint');
        $('.logo').animate({
            right : '12%'

        } ,1000, 'easeInQuint', function(){
            $('#next_3').fadeIn();
        }); 

    });
    $('#next_3').click(function(){
        window.location.replace("http://madeupdesign.it/home/");
        //$('#intro_container').fadeOut();

    });
}

introAnimation();