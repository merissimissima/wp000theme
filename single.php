<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header();
get_template_part( 'partials/content', 'openbody' );?>

<div class="page single news">
    <div class="section content-s">
        <div class="grid">
                <?php while ( have_posts() ) : the_post(); ?>

		<div class="row">
			<div class="slot slot-0-1-2-3">
                <div class="w-title"><h2 class="comics">Avvisi</h2></div>
                <article class="news">
                    <div class="newscontent">
                        <div class="newsdata newsauthor ">
                            <span class="day"><?php the_date("d");  ?></span> <span class="month"><?php the_time("F"); ?></span> <span class="month"><?php the_time("Y"); ?></span>
                            </span>
                        </div>
                        <h2> <?php the_title(); ?></h2>
                        <div class="text_content"><?php the_content(); ?></div>
                    </div>
                </article>
			</div>
            <div class="slot slot-4-5 sidebar color_gray">
                <div class="w-content">
                    <?php get_sidebar('sidebar-main');?>
                    <?php //get_template_part( 'partials/content', 'nextevents' );?>
                </div>
            </div><!-- chiude sidebar -->

        </div>
		<?php endwhile; // end of the loop. ?>	
			

	</div><!-- end grid-->
            </div>
</div><!-- end page -->
<?php get_footer(); ?>


