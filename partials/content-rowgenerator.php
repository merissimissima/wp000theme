<?php
function checkColorSlot($slot){
    //<?php
    $colorCol = get_sub_field($slot.'_color');
    $sidebar = get_sub_field($slot.'_sidebar');

     if($colorCol != '0'){ echo 'color_'.$colorCol.'"><div class="w-content';}?>
                        ">
     <?php
    if($sidebar==1){
       get_sidebar();
    }
    else{
        the_sub_field($slot);
    }
     ?>
     <?php if($colorCol != '0'){ echo '</div>'; }
}
/*1c : 1 Colonna
2c_sb : 2 Colonne - stretta+larga
2c_bs : 2 Colonne - larga+stretta
2c : 2 Colonne uguali
3c : 3 Colonne uguali
3c_b_s_s : 3 Colonne - grande + piccola + piccola
3c_s_s_b : 3 Colonne - piccola + piccola + grande
4c : 4 Colonne uguali
4c_bs : 4 Colonne 1 grande 3 piccole
4c_sbsb : 4 Colonne - piccola + grande + piccola + grande
6c : 6 Colonne uguali*/

if( have_rows('riga') ): ?>
    <?php while ( have_rows('riga') ) : the_row();
        $rowType = get_sub_field('row_type'); ?>
        <div class="row">
            <?php switch($rowType){
                case "1c":?>
                    <div class="slot slot-0-1-2-3-4-5  <?php the_sub_field('col_1_customclass');?> <?php checkColorSlot('col_1'); ?>
                    </div>
                      <?php   break;
                case "2c_sb":?>
                    <div class="slot slot-6 <?php the_sub_field('col_1_customclass');?> <?php checkColorSlot('col_1'); ?></div>
                    <div class="slot slot-7-8-9 <?php the_sub_field('col_2_customclass');?> <?php checkColorSlot('col_2'); ?></div>
                    <?php  break;
                case "2c_bs":?>
                    <div class="slot slot-6-7-8 <?php the_sub_field('col_1_customclass');?> <?php checkColorSlot('col_1'); ?></div>
                    <div class="slot slot-9 <?php the_sub_field('col_2_customclass');?> <?php checkColorSlot('col_2'); ?></div>
                    <?php  break;
                case "2c":?>
                    <div class="slot slot-6-7 <?php the_sub_field('col_1_customclass');?> <?php checkColorSlot('col_1'); ?></div>
                    <div class="slot slot-8-9 <?php the_sub_field('col_2_customclass');?> <?php checkColorSlot('col_2'); ?></div>
                    <?php  break;
                case "3c":?>
                    <div class="slot slot-0-1 <?php the_sub_field('col_1_customclass');?> <?php checkColorSlot('col_1'); ?></div>
                    <div class="slot slot-2-3 <?php the_sub_field('col_2_customclass');?> <?php checkColorSlot('col_2'); ?></div>
                    <div class="slot slot-4-5 <?php the_sub_field('col_3_customclass');?> <?php checkColorSlot('col_3'); ?></div>
                    <?php  break;
                case "3c_b_s_s":?>
                    <div class="slot slot-6-7 <?php the_sub_field('col_1_customclass');?> <?php checkColorSlot('col_1'); ?></div>
                    <div class="slot slot-8 <?php the_sub_field('col_2_customclass');?> <?php checkColorSlot('col_2'); ?></div>
                    <div class="slot slot-9 <?php the_sub_field('col_3_customclass');?> <?php checkColorSlot('col_3'); ?></div>
                    <?php  break;
                case "3c_s_s_b":?>
                    <div class="slot slot-6 <?php the_sub_field('col_1_customclass');?> <?php checkColorSlot('col_1'); ?></div>
                    <div class="slot slot-7 <?php the_sub_field('col_2_customclass');?> <?php checkColorSlot('col_2'); ?></div>
                    <div class="slot slot-8-9 <?php the_sub_field('col_3_customclass');?> <?php checkColorSlot('col_3'); ?></div>
                    <?php  break;
                case "4c":?>
                    <div class="slot slot-6 <?php the_sub_field('col_1_customclass');?> <?php checkColorSlot('col_1'); ?></div>
                    <div class="slot slot-7 <?php the_sub_field('col_2_customclass');?> <?php checkColorSlot('col_2'); ?></div>
                    <div class="slot slot-8 <?php the_sub_field('col_3_customclass');?> <?php checkColorSlot('col_3'); ?></div>
                    <div class="slot slot-9 <?php the_sub_field('col_4_customclass');?> <?php checkColorSlot('col_4'); ?></div>
                    <?php  break;
                case "4c_bs":?>
                    <div class="slot slot-0-1-2 <?php the_sub_field('col_1_customclass');?> <?php checkColorSlot('col_1'); ?></div>
                    <div class="slot slot-3 <?php the_sub_field('col_2_customclass');?> <?php checkColorSlot('col_2'); ?></div>
                    <div class="slot slot-4 <?php the_sub_field('col_3_customclass');?> <?php checkColorSlot('col_3'); ?></div>
                    <div class="slot slot-5 <?php the_sub_field('col_4_customclass');?> <?php checkColorSlot('col_4'); ?></div>
                    <?php  break;
                case "4c_sbsb":?>
                <div class="slot slot-0 <?php the_sub_field('col_1_customclass');?> <?php checkColorSlot('col_1'); ?></div>
                    <div class="slot slot-1-2 <?php the_sub_field('col_2_customclass');?> <?php checkColorSlot('col_2'); ?></div>
                    <div class="slot slot-3 <?php the_sub_field('col_3_customclass');?> <?php checkColorSlot('col_3'); ?></div>
                    <div class="slot slot-4-5 <?php the_sub_field('col_4_customclass');?> <?php checkColorSlot('col_4'); ?></div>
                    <?php  break;
                case "6c":?>
                    <div class="slot slot-0 <?php the_sub_field('col_1_customclass');?> <?php checkColorSlot('col_1'); ?></div>
                    <div class="slot slot-1 <?php the_sub_field('col_2_customclass');?> <?php checkColorSlot('col_2'); ?></div>
                    <div class="slot slot-2 <?php the_sub_field('col_3_customclass');?> <?php checkColorSlot('col_3'); ?></div>
                    <div class="slot slot-3 <?php the_sub_field('col_4_customclass');?> <?php checkColorSlot('col_4'); ?></div>
                    <div class="slot slot-4 <?php the_sub_field('col_4_customclass');?> <?php checkColorSlot('col_5'); ?></div>
                    <div class="slot slot-5 <?php the_sub_field('col_4_customclass');?> <?php checkColorSlot('col_6'); ?></div>

                    <?php  break;


            }?>
        </div>
    <?php endwhile; ?>
<?php endif; ?>