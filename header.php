<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<!-- * meta * -->	
<meta charset="<?php bloginfo( 'charset' ); ?>" />

<!-- style -->

<!-- mobile -->
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">

<title><?php
	
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );
?>
</title>
	


<!-- + Analytics + -->
<?php
	wp_head();
?>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendors/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendors/jquery.isotope.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendors/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendors/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendors/imagesloaded.pkgd.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendors/jquery.fancybox.js" type="text/javascript"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style/vendors/jquery.fancybox.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/siteStyle.css" type="text/css"/>
    <script type="text/javascript">
        $(document).ready(function() {
           $('.fancybox').fancybox();
        });

    </script>
    <!--  fix ie -->
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/crossbrowsing/html5.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style/modules/ie.css">


    <![endif]-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
       // setTimeout("_gaq.push(['_trackEvent', '15_seconds', 'read'])",15000);
        ga('create', 'UA-52807543-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');

    </script>
    <script>(function() {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
            _fbq.push(['addPixelId', '722121761157369']);
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=722121761157369&amp;ev=NoScript" /></noscript>
</head>
