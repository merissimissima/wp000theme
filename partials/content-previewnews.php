<div class="news preview">

    <?php
    if ( has_post_thumbnail() ) { // controlla se il post ha un'immagine in evidenza assegnata.?>
        <div class="newsimage">
             <?php the_post_thumbnail('thumbnail'); ?>
        </div>
    <?php } ?>

    <div class="newscontent <?php if ( has_post_thumbnail() ) { echo 'mleft'; } ?>">
        <div class="newsdata  ">
            <span class="day"><?php the_date("d");  ?></span> <span class="month"><?php the_time("F"); ?></span> <span class="month"><?php the_time("Y"); ?></span>
        </div>
        <div class="excerpt">
            <h3> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <div class="text_content"><?php
                if($longExcertp==1){
                    the_excerpt(); ?>
                    <a href="<?php the_permalink(); ?>" class="readmore">Continua...</a>
                <?php
                }else{
                    the_field('home_excerpt');
                }
                 ?>

            </div>
        </div>
    </div>
</div>