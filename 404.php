<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header();
get_template_part( 'content', 'openbody' );  ?>

<div class="page nofound">
	<div class="grid">
		
		<div class="row">
			<div class="slot slot-0-1-2-3">
			
				<h1>Il contenuto che stai cercando, non è al momento disponibile</h1>
				<div class="only-mobile"><?php dynamic_sidebar( 'mobile-sidebar' ); ?></div>
				
		
			</div>
			<div class="slot slot-4-5 no-mobile slot">
				<?php get_sidebar();?>
			</div>

		</div><!-- end row-->
	</div><!-- end grid-->
</div><!-- end page --> 
<?php get_footer(); ?>
