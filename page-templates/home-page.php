<?php
/**
 * Template Name: Home
 * review Mery @ Acqua Liofilizzata
 */
get_header();
get_template_part( 'partials/content', 'openbody' );
$args = array(    'post_type' => 'furniture');
$query = new WP_Query($args);
$totalFurniture =  $query->found_posts;

if(!isset($_COOKIE['intro'])){
   header('location:http://madeupdesign.it/concept-madeup-design/');
 }


?>
<div class="page home-template">
    <div class="grid">
        <div class="row">
            <div class="slot slot-0-1-2">
                <div class="html_carousel">
                    <div id="detailcarousel">
                        <?php while ( have_rows('home_carousel') ) : the_row();

                            $foto = get_sub_field('img_car');


                            ?>
                            <div class="slide">
                                <img class="alignnone" alt="madeup" src="<?php echo $foto['sizes']['carousel-size']; ?>">
                            </div>

                        <?php endwhile; ?>



                    </div>
                    <div  id="prev"><</div>
                    <div  id="next">></div>
                </div>



            </div>
            <div class="slot slot-3-4-5 main-logo" ><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-main.png" alt="logo made up"/> </a></div>
        </div>
        <div id="container" class="isotope">

            <?php $args = array(    'post_type' => 'furniture',    'posts_per_page' => 1);
            $query = new WP_Query($args);
            while ($query->have_posts()) : $query->the_post();
                get_template_part('partials/content','furniture-small');
            endwhile; wp_reset_postdata();
            ?>

            <div class="c2 item info">
                <div class="content col-1 col">
                    <h1><?php the_field('b1_c1_title');?></h1>
                    <?php the_field('b1_c1_content');?>
                </div>
                <div class="content col-2 col">
                    <?php the_field('b1_c2_content');?>

                </div>

            </div>

            <?php $args = array(    'post_type' => 'furniture',    'posts_per_page' => 2, 'offset'=>1);
            $query = new WP_Query($args);
            while ($query->have_posts()) : $query->the_post();
                get_template_part('partials/content','furniture-small');
            endwhile; wp_reset_postdata();
            ?>
            <div class="c1 item info">
                <div class="content col">
                    <h1><?php the_field('b2_title');?></h1>
                    <?php the_field('b2_content');?>
                </div>
            </div>




          </div>
        <div class="load_more"> <a href="#" id="another" class="button more"><span class="plus">+</span><span class="more">more</span> </a> </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){


       function initializeIsotope($container){
            $container.imagesLoaded(function() {
                $container.isotope({
                    resizable: false
                });
            });
        }

        var $container = $('#container');
        initializeIsotope($container);


        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })();
        $(window).resize(function(){

            delay(function(){
                initializeIsotope($container);
            }, 500);
        });


        var offset = 3;
        $("#another").click(function(){
             $.get("<?php echo get_template_directory_uri(); ?>/more/more-furniture.php?offset="+offset, function(data){
                 var newItems = $(data).filter("div.item");
                 $('#container').append( newItems );
                 newItems.hide();
                 newItems.imagesLoaded(function(){
                    newItems.fadeIn();
                    $('#container').isotope( 'appended', newItems).isotope('reLayout', function(){});
                 });


                 offset = offset+3;

                  if((offset) > <?php echo $totalFurniture; ?>){
                     $("#another").fadeOut();
                 }

            });
            return false;
        });
        $("#detailcarousel").carouFredSel({
            responsive	: true,
            scroll		: {
                fx              : "cover-fade",
                easing          : "swing",
                duration: 500

            },
            items		: {
                visible		: 1,
                width		: 794,
                height		: "64%"
            },
            auto : {
                pauseOnHover : true,
                duration: 1000
            },
            onCreate: function (data) {
                console.log('create ');
                $('.html_carousel div.slide').css('visibility', 'visible');
            },
            prev: '#prev',
            next: '#next'
        });
    });

</script>
    <?php get_footer(); ?>
