<?php
/*
Template Name: Pagina a colonne
*/
get_header();
get_template_part( 'partials/content', 'openbody' );
?>

    <div class="section colpage">
        <div class="grid">
            <?php  get_template_part( 'partials/content', 'rowgenerator' );?>
        </div>

    </div>
    <!-- #content -->

<?php

	get_footer(); ?>