<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

<div class="footer">
	<div class="grid">
		<div class="row ">
                            <?php get_sidebar ('footer'); ?>

		</div>
	</div><!-- chiude grid -->
</div>
<?php wp_footer(); ?>

</body>
</html>