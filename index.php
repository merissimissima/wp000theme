<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header();
get_template_part( 'partials/content', 'openbody' );  ?>

<div class="page news-list">
    <div class="section content-s">
        <div class="grid">
           <div class="row">
               <div class="slot slot-0-1-2-3">
                   <h2>Avvisi</h2>
                   <?php if ( have_posts() ) : ?>

                       <?php /* The loop */ ?>
                       <?php while ( have_posts() ) : the_post(); ?>
                           <div class="row innerrow border_down">
                            <?php  $longExcertp = 1;  include(locate_template('partials/content-previewnews.php'));  ?>
                           </div>
                       <?php endwhile; ?>

                       <?php //twentythirteen_paging_nav(); ?>

                   <?php else : ?>
                       dfsfa
                       <?php get_template_part( 'partials/content', 'none' ); ?>
                   <?php endif; ?>
               </div><!-- chiude main left content -->
               <div class="slot slot-4-5 sidebar color_gray">
                   <div class="w-content">
                   <?php get_sidebar('sidebar-main');?>
                   <?php //get_template_part( 'partials/content', 'nextevents' );?>
                   </div>
               </div><!-- chiude sidebar -->
         </div><!-- chiude row -->
    </div><!-- chiude grid  -->
</div><!-- chiude section -->
</div>
<?php get_footer(); ?>