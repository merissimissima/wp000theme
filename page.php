<?php


get_header();
get_template_part( 'partials/content', 'openbody' );
?>

<div class="page">
    <div class="grid">
        <div class="row">
            <div class="slot  slot-6-7-8 text">
                <?php while ( have_posts() ) : the_post(); ?>
                    <h1><?php the_title();?></h1>
                    <?php the_content(); ?>
                <?php endwhile; ?>
                </div>
            <div class="slot slot-9"><?php get_sidebar(); ?></div>
        </div><!-- chiude row -->
    </div><!-- chiude grid -->
</div><!-- chiude page -->


<?php get_footer(); ?>
