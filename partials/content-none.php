<article id="post-0" class="post no-results not-found">
				<header >
					<h1 ><?php _e( 'No posts to display', 'twentytwelve' ); ?></h1>
				</header>
			<div class="content">
					<p><?php printf( __( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'twentytwelve' ), admin_url( 'post-new.php' ) ); ?></p>
				</div><!-- .entry-content -->
</article>